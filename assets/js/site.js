$(document).ready(function() {
	// $('.banner').slick({
    //     slide: 'figure',
    //     autoplay: true,
    //     dotsClass: 'custom_paging',
    //     customPaging: function (slider, i) {
    //         //FYI just have a look at the object to find available information
    //         //press f12 to access the console in most browsers
    //         //you could also debug or look in the source
    //         console.log(slider);
    //         return  (i + 1) + '/' + slider.slideCount;
    //     }
    // });
    $('.banner').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        fade: true,
        asNavFor: '.thumbnail-slide'
      });
      $('.thumbnail-slide').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.banner',
        arrows:false,
        focusOnSelect: true
      });
});
